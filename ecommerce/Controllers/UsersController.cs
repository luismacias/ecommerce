﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ecommerce.Clases;
using ecommerce.Models;

namespace ecommerce.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.Include(u => u.City).Include(u => u.Company).Include(u => u.Department);
            return View(users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.Cityid = new SelectList(CombosHelper.GetCities(), "Cityid", "Name");
            ViewBag.Companyid = new SelectList(CombosHelper.GetCompanies (), "Companyid", "Name");
            ViewBag.Departmentid = new SelectList(CombosHelper.GetDepartments(), "Departmentid", "Name");
            return View();
        }

        // POST: Users/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                UsersHelper.CreateUserASP(user.UserName, "User");
                
                if (user.PhotoFile != null)
                {

                    var folder = "~/Content/Users";
                    var file = string.Format("{0}.jpg", user.UserId);
                    var response = FilesHelper.UploadPhoto(user.PhotoFile, folder, file);
                    if (response)
                    {

                        var pic = string.Format("{0}/{1}", folder, file);
                        user.Photo = pic;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }        
                return RedirectToAction("Index");
            }

            ViewBag.Cityid = new SelectList(CombosHelper.GetCities(), "Cityid", "Name", user.Cityid);
            ViewBag.Companyid = new SelectList(CombosHelper.GetCompanies(), "Companyid", "Name", user.Companyid);
            ViewBag.Departmentid = new SelectList(CombosHelper.GetDepartments(), "Departmentid", "Name", user.Departmentid);
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cityid = new SelectList(CombosHelper.GetCities(), "Cityid", "Name", user.Cityid);
            ViewBag.Companyid = new SelectList(CombosHelper.GetCompanies(), "Companyid", "Name", user.Companyid);
            ViewBag.Departmentid = new SelectList(CombosHelper.GetDepartments(), "Departmentid", "Name", user.Departmentid);
            return View(user);
        }

        // POST: Users/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {



            if (ModelState.IsValid)
            {
                if (user.PhotoFile != null)
                {

                    var folder = "~/Content/Users";
                    var file = string.Format("{0}.jpg", user.UserId);
                    var response = FilesHelper.UploadPhoto(user.PhotoFile, folder, file);
                    var pic = string.Format("{0}/{1}", folder, file);
                    user.Photo = pic;
                     
                    }

                var db2 = new ECommerceContext();
                var currentUser = db2.Users.Find(user.UserId);
                if (currentUser.UserName != user.UserName)
                {
                    UsersHelper.UpadateUserName(currentUser.UserName, user.UserName);


                }
                db2.Dispose();


             db.Entry(user).State = EntityState.Modified;
             db.SaveChanges();  
             return RedirectToAction("Index");
            }

            ViewBag.Cityid = new SelectList(CombosHelper.GetCities(), "Cityid", "Name", user.Cityid);
            ViewBag.Companyid = new SelectList(CombosHelper.GetCompanies(), "Companyid", "Name", user.Companyid);
            ViewBag.Departmentid = new SelectList(CombosHelper.GetDepartments(), "Departmentid", "Name", user.Departmentid);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            try
            {
                db.SaveChanges();
            }
            catch (Exception) 
            {

                throw;
            }
            UsersHelper.DeleteUser(user.UserName);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetCities(int departmentId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cities = db.Cities.Where(m => m.Departmentid == departmentId);
            return Json(cities);
        }
    }
}
