﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class ECommerceContext: DbContext 
    {
        public ECommerceContext() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
        public DbSet<Department> Departments { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.Company> Companies { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.Tax> Taxes { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<ecommerce.Models.Warehouse> Warehouses { get; set; }
        
        public DbSet<Inventory> Inventories { get; set; }
        //   public System.Data.Entity.DbSet<ecommerce.Models.Category> Categories { get; set; }
    }
}