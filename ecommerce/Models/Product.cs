﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }



        [Index("Product_Companyid_ BarCode_Index", 1, IsUnique = true)]
        [Index("Product_Companyid_Description_Index", 1, IsUnique = true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Company"))]
        public int Companyid { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe ser de {1} maximo")]
        [Display(Name = ("Product"))]
        [Index("Product_Companyid_Description_Index", 2, IsUnique = true)]

        public string Description { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(13, ErrorMessage = "El campo {0} debe ser de {1} maximo")]
        [Display(Name = (" BarCode"))]
        [Index("Product_Companyid_ BarCode_Index", 2, IsUnique = true)]
        public string BarCode { get; set; }

        [Display(Name = ("Category"))]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        public int CategoryId { get; set; }

        [Display(Name = ("Tax"))]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        public int TaxId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(0, double.MaxValue, ErrorMessage = "You must select a {0} entre 0 y 1")]
        public decimal Price { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }
        [NotMapped]
        [Display(Name = ("Image"))]
        public HttpPostedFileBase ImageFile { get; set; }


        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public double Stock { get { return Inventories == null ? 0 : Inventories.Sum(i => i.Stock); } }
        public virtual Company Company { get; set; }
        public virtual Category Category { get; set; }
        public virtual Tax Tax { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
    }
}