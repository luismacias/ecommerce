﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

       // [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(256, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "E-mail")]
        [Index("User_Name_Index", IsUnique = true)]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "First name")]

        public string FirstName { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }



        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Telephone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        public string Address { get; set; }

        //  [Required(ErrorMessage = "El campo {0} es requerido")]
        //  [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        [DataType(DataType.ImageUrl)]
        public string Photo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Deparment"))]
        public int Departmentid { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("City"))]
        public int Cityid { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Company"))]
        public int Companyid { get; set; }
        [Display (Name ="User")]
         public string FullName { get { return string.Format("{0} {1}",FirstName,LastName); } }




        [NotMapped]
        public HttpPostedFileBase PhotoFile { get; set; } 



        public virtual Department Department { get; set; }
        public virtual City City { get; set; }

        public virtual Company Company { get;set; }



    }
}