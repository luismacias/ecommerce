﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models
{

    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe ser de {1} maximo")]
        [Display(Name = ("Category"))]
        [Index("Category_Companyid_Description_Index", 2, IsUnique = true)]
        public string Description { get; set; }


        [Index("Category_Companyid_Description_Index", 1, IsUnique = true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Company"))]
        //public int Departmentid { get; set; }
        
        public int Companyid { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}