﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models
{
    public class City
    {
        
        [Key]
        public int Cityid { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe ser de {1} maximo")]
        [Display(Name=("City"))]
        [Index("City_DepartmentidName_Index", 2,IsUnique =true)]
        public string Name { get; set; }


        [Index("City_DepartmentidName_Index", 1, IsUnique = true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1,double.MaxValue,ErrorMessage ="You must select a {0}")]
        public int Departmentid { get; set; }


 
        ///// ///////
        ///
        public virtual Department Department { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<User> Users { get; set; }


        public virtual ICollection<Warehouse> Warehouses { get; set; }
    }
}