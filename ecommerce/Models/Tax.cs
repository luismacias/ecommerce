﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class Tax
    {

        [Key]
        public int TaxId { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe ser de {1} maximo")]
        [Display(Name = ("Tax"))]
        [Index("Tax_Companyid_Description_Index", 2, IsUnique = true)]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:P2}", ApplyFormatInEditMode = false)]
        [Range(0,1,ErrorMessage = "You must select a {0} entre 0 y 1")]
        public double Rate { get; set; }




        [Index("Tax_Companyid_Description_Index", 1, IsUnique = true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Company"))]
        //public int Departmentid { get; set; }

        public int Companyid { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
