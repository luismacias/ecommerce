﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class Warehouse
    {
        [Key]
        public int WarehouseId { get; set; }



        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Company"))]
        [Index("Warehouse_Companyid_Name_Index", 1, IsUnique = true)]
        public int Companyid { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(256, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Warehouse")]
        [Index("Warehouse_Companyid_Name_Index", 2, IsUnique = true)]

        public string Name { get; set; }




        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Telephone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} es requerido")]
        public string Address { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("Department"))]
        public int Departmentid { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = ("City"))]
        public int Cityid { get; set; }

        public virtual Department Department { get; set; }
        public virtual City City { get; set; }

        public virtual Company Company { get; set; }
    
    
    }

}